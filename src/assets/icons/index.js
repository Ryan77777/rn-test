import IcPlus from './IcPlus';
import IcArrowLeft from './IcArrowLeft';
import IcTrash from './IcTrash';
import IcCamera from './IcCamera';

export {IcPlus, IcArrowLeft, IcTrash, IcCamera};
