import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import FlashMessage from 'react-native-flash-message';
import {ModalPortal} from 'react-native-modals';

const App = () => {
  return (
    <NavigationContainer>
      <Router />
      <FlashMessage position="top" />
      <ModalPortal />
    </NavigationContainer>
  );
};

export default App;
