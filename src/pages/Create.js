import {
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {
  BottomSheet,
  Button,
  Container,
  Gap,
  Header,
  TextInput,
  TextRegular,
} from '../components';
import {colors, scaleSize} from '../utils';
import Animated, {FadeIn, FadeOut} from 'react-native-reanimated';
import {IcTrash} from '../assets';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {showMessage} from 'react-native-flash-message';
import Geolocation from '@react-native-community/geolocation';
import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob';

const BASE_URL = 'http://192.168.100.183:3000';

const Create = ({navigation}) => {
  const [state, setState] = useState({
    name: '',
    photo: '',
    location: '',
    isErrorPhoto: false,
  });
  const [isShow, setIsShow] = useState(false);

  const deleteImage = () => {
    setState({
      ...state,
      photo: '',
    });
  };

  const takePhoto = async () => {
    const maxSize = 5000000;
    await launchCamera({mediaType: 'photo'}, response => {
      if (response.didCancel || response.error) {
        showMessage({
          message: 'Oops, sepertinya anda tidak memilih foto nya?',
          type: 'danger',
        });
      } else {
        if (response.assets[0].fileSize > maxSize) {
          setState({
            ...state,
            isErrorPhoto: true,
          });
        } else {
          setIsShow(false);
          setState({
            ...state,
            photo: response,
          });
        }
      }
    });
  };

  const chooseFromGallery = async () => {
    const maxSize = 5000000;
    await launchImageLibrary({mediaType: 'photo'}, response => {
      if (response.didCancel || response.error) {
        showMessage({
          message: 'Oops, sepertinya anda tidak memilih foto nya?',
          type: 'danger',
        });
      } else {
        if (response.assets[0].fileSize > maxSize) {
          setState({
            ...state,
            isErrorPhoto: true,
          });
        } else {
          setIsShow(false);
          setState({
            ...state,
            photo: response,
          });
        }
      }
    });
  };

  const getCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        setState({
          ...state,
          location: `${position.coords.latitude}, ${position.coords.longitude}`,
        });
      },
      error =>
        showMessage({
          message: 'Lokasi anda tidak ditemukan',
          type: 'danger',
        }),
      {enableHighAccuracy: true},
    );
  };

  const checkDisabled = () => {
    const {name, photo, location} = state;
    const form = {name, photo, location};
    const dataInputEmpty = Object.keys(form).filter(data => form[data] === '');
    const dataEmpty = [...dataInputEmpty];

    if (dataEmpty.length) {
      for (let index = 0; index < dataEmpty.length; index++) {
        return true;
      }
    } else {
      return false;
    }
  };

  const onSubmit = async () => {
    try {
      const formData = new FormData();
      const dataImg =
        Platform.OS === 'ios'
          ? RNFetchBlob.wrap(state.photo.assets[0].uri).replace('file://', '')
          : RNFetchBlob.wrap(state.photo.assets[0].uri);
      formData.append('name', state.name);
      formData.append('file', {
        uri: state.photo.assets[0].uri,
        type: state.photo.assets[0].type,
        name: state.photo.assets[0].fileName,
        path: dataImg,
      });
      formData.append('location', state.location);

      const response = await axios.post(`${BASE_URL}/data`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      if (response.status === 200) {
        showMessage({
          message: 'Data berhasil disimpan',
          type: 'success',
        });
        setState({
          name: '',
          photo: '',
          location: '',
          isErrorPhoto: false,
        });
      }
    } catch (error) {
      console.log(error);
      showMessage({
        message: 'Something went wrong',
        type: 'danger',
      });
    }
  };

  const handleSheetChanges = () => {
    setIsShow(true);
  };

  return (
    <Container>
      <Header />
      <View style={styles.container}>
        <TextInput
          label="Nama kegiatan"
          placeholder="Masukan Nama Kegiatan"
          value={state.name}
          onChangeText={value =>
            setState({
              ...state,
              name: value,
            })
          }
          isRequired
        />
        {state.photo ? (
          <>
            <TextRegular
              type="Text 14"
              text="Upload foto kegiatan"
              color={colors['Neutral-800']}
            />
            <Gap height={8} />
            <Animated.View
              entering={FadeIn.duration(2000)}
              exiting={FadeOut.duration(2000)}
              style={styles.uploadWrapper}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.button}
                onPress={deleteImage}>
                <IcTrash />
              </TouchableOpacity>
              <Image
                source={{uri: state.photo.assets[0].uri}}
                style={styles.profile}
              />
            </Animated.View>
            <Gap height={20} />
          </>
        ) : (
          <TextInput
            type="file"
            label="Upload foto kegiatan"
            onPress={handleSheetChanges}
            isRequired
            isError={state.isErrorPhoto}
            errorMessage="Ukuran foto maksimal 5mb. Coba pakai foto lain ya"
          />
        )}
        <TextInput
          type="location"
          label="Lokasi kegiatan"
          value={state.location}
          onPress={() => getCurrentPosition()}
          isRequired
        />
        <Gap height={40} />
        <Button
          isPrimary
          disable={checkDisabled()}
          text="Simpan"
          onPress={onSubmit}
        />
      </View>

      {/* BottomSheet */}
      <BottomSheet
        show={isShow}
        onClose={() => setIsShow(false)}
        pickCamera={() => {
          takePhoto();
        }}
        pickGalery={() => {
          chooseFromGallery();
        }}
      />
    </Container>
  );
};

export default Create;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: scaleSize(24),
  },
  uploadWrapper: {
    height: (Dimensions.get('window').height * 35) / 100,
    borderRadius: scaleSize(8),
    borderWidth: 1.5,
    borderStyle: 'dashed',
    borderColor: colors['Neutral-200'],
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  profile: {
    width: '100%',
    height: (Dimensions.get('window').height * 35) / 100,
    borderRadius: scaleSize(8),
  },
  button: {
    position: 'absolute',
    top: 16,
    left: 16,
    backgroundColor: colors['White-0'],
    width: scaleSize(32),
    height: scaleSize(32),
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
});
