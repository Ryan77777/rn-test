import {Dimensions, FlatList, Image, StyleSheet, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  Button,
  Container,
  FloatingButton,
  Gap,
  TextBold,
  TextLight,
  TextRegular,
} from '../components';
import {colors, scaleSize} from '../utils';
import Animated, {FadeInLeft, Layout} from 'react-native-reanimated';
import {showMessage} from 'react-native-flash-message';
import axios from 'axios';
import Modal from 'react-native-modal';

const BASE_URL = 'http://192.168.100.183:3000';

const RenderItem = ({data, index, onPress}) => (
  <Animated.View
    style={styles.list}
    entering={FadeInLeft.delay(100 * index)}
    layout={Layout.delay(100)}>
    <View style={styles}>
      <TextRegular type="Text 16" text={data?.name} color={colors['Black-0']} />
      <TextLight
        type="Text 12"
        text={`${data?.latitude} - ${data?.longitude}`}
        color={colors['Black-0']}
      />
    </View>
    <Button text="Lihat" onPress={onPress} />
  </Animated.View>
);

const RenderEmpty = () => (
  <View style={styles.emptyWrapper}>
    <TextRegular
      type="Text 14"
      text="Data tidak tersedia"
      color={colors['Black-0']}
    />
  </View>
);

const List = ({navigation}) => {
  const [data, setData] = useState([]);

  const [currentIndex, setCurrentIndex] = useState(null);
  const [isModalVisible, setModalVisible] = useState(false);

  const showModal = id => {
    setModalVisible(!isModalVisible);
    setCurrentIndex(id);
  };

  const hideModal = id => {
    setModalVisible(!isModalVisible);
    setCurrentIndex(null);
  };

  const getData = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/data`);
      if (response.status === 200) {
        setData(response.data.data);
      }
    } catch (error) {
      showMessage({
        message: 'Data berhasil disimpan',
        type: 'success',
      });
    }
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <Container>
      <FlatList
        data={data}
        keyExtractor={item => item.id}
        contentContainerStyle={styles.container}
        renderItem={({item, index}) => (
          <RenderItem
            key={index}
            data={item}
            index={index}
            onPress={() => showModal(index)}
          />
        )}
        ListEmptyComponent={() => <RenderEmpty />}
        ListHeaderComponent={() => (
          <>
            <TextBold
              type="Text 32"
              text="Daftar Kegiatan"
              color={colors['Black-0']}
            />
            <Gap height={24} />
          </>
        )}
      />
      <Modal isVisible={isModalVisible} onBackdropPress={hideModal}>
        <View style={styles.modal}>
          <Image
            source={{uri: data?.[currentIndex]?.photo}}
            style={styles.image}
          />
        </View>
      </Modal>
      <FloatingButton onPress={() => navigation.navigate('Create')} />
    </Container>
  );
};

export default List;

const styles = StyleSheet.create({
  container: {
    padding: scaleSize(24),
  },
  list: {
    backgroundColor: colors['White-0'],
    borderWidth: 1,
    borderRadius: scaleSize(8),
    borderColor: colors['Neutral-200'],
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(12),
    marginBottom: scaleSize(16),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  emptyWrapper: {
    flex: 1,
    height: Dimensions.get('window').height - 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal: {
    backgroundColor: colors['White-0'],
    borderWidth: 2,
    borderColor: colors['White-0'],
  },
  image: {
    width: '100%',
    height: Dimensions.get('window').height * 0.4,
  },
});
