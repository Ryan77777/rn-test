import React, {useEffect} from 'react';
import {Image, StyleSheet} from 'react-native';
import {FadeIn, Transition} from 'react-native-reanimated';
import {Logo} from '../assets';
import {Container} from '../components';
import {scaleSize} from '../utils';

const Splashscreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('List');
    }, 2000);
  }, [navigation]);

  return (
    <Container center entering={FadeIn.duration(1000)} layout={Transition}>
      <Image source={Logo} style={styles.logo} />
    </Container>
  );
};

export default Splashscreen;

const styles = StyleSheet.create({
  logo: {
    width: scaleSize(120),
    height: scaleSize(120),
  },
});
