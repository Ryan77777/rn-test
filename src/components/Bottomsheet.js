import React from 'react';
import {StyleSheet, View, Dimensions, TouchableOpacity} from 'react-native';
import {Modal, SlideAnimation} from 'react-native-modals';
import {TextRegular, TextBold} from './Text';
import {colors, scaleSize} from '../utils';
import Gap from './Gap';
const screensWidth = Dimensions.get('window').width;

const BottomSheet = ({
  show,
  onClose,
  pickCamera,
  pickGalery,
  modalStyle,
  style,
}) => {
  return (
    <Modal
      visible={show}
      modalAnimation={
        new SlideAnimation({
          slideFrom: 'bottom',
        })
      }
      style={styles.container}
      modalStyle={[modalStyle, styles.ModalChild]}
      onHardwareBackPress={() => {
        onClose();
        return true;
      }}
      onTouchOutside={() => {
        onClose();
      }}
      onSwipeOut={() => {
        onClose();
      }}
      swipeDirection={['down']}>
      <View style={[styles.PickImageWrapper, style]}>
        <TextBold
          type="Text 14"
          text="Upload foto kegiatan"
          color={colors['Neutral-800']}
        />
        <Gap height={24} />
        <TouchableOpacity onPress={pickCamera}>
          <TextRegular
            type="Text 14"
            text="Ambil Foto"
            color={colors['Neutral-800']}
          />
        </TouchableOpacity>
        <Gap height={12} />
        <TouchableOpacity onPress={pickGalery}>
          <TextRegular
            text="Pilih foto yang ada"
            type="Text 14"
            color={colors['Neutral-800']}
          />
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

export default BottomSheet;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
  },
  ModalChild: {
    backgroundColor: null,
    borderRadius: null,
    flex: 1,
    width: screensWidth,
  },
  PickImageWrapper: {
    position: 'absolute',
    bottom: 0,
    width: screensWidth,
    backgroundColor: colors['White-0'],
    borderTopLeftRadius: scaleSize(8),
    borderTopRightRadius: scaleSize(8),
    paddingVertical: scaleSize(16),
    paddingHorizontal: scaleSize(24),
    elevation: 30,
  },
});
