import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {colors} from '../utils';

const Container = ({children, center}) => {
  return (
    <SafeAreaView style={styles.container(center)}>{children}</SafeAreaView>
  );
};

export default Container;

const styles = StyleSheet.create({
  container: center => ({
    flex: 1,
    justifyContent: center && 'center',
    alignItems: center && 'center',
    backgroundColor: colors['White-0'],
  }),
});
