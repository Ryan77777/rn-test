import {StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import {colors, scaleSize} from '../utils';
import {IcPlus} from '../assets';

const FloatingButton = ({onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.button}
      onPress={onPress}>
      <IcPlus />
    </TouchableOpacity>
  );
};

export default FloatingButton;

const styles = StyleSheet.create({
  button: {
    position: 'absolute',
    bottom: scaleSize(24),
    right: scaleSize(24),
    width: scaleSize(40),
    height: scaleSize(40),
    backgroundColor: colors['Primary-500'],
    borderRadius: scaleSize(20),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
