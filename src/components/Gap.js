import React from 'react';
import {StyleSheet, View} from 'react-native';
import {scaleSize} from '../utils';

const Gap = ({width = 0, height = 0}) => {
  return <View style={styles.gap(width, height)} />;
};

const styles = StyleSheet.create({
  gap: (width, height) => ({
    width: scaleSize(width),
    height: scaleSize(height),
  }),
});

export default Gap;
