import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, scaleSize} from '../utils';
import {TextRegular} from './Text';
import Gap from './Gap';
import {IcArrowLeft} from '../assets';
import {useNavigation} from '@react-navigation/native';

const Header = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <IcArrowLeft width={16} height={16} />
      </TouchableOpacity>
      <Gap width={16} />
      <TextRegular
        type="Text 16"
        text="Tambah Data"
        color={colors['Black-0']}
      />
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  header: {
    padding: scaleSize(24),
    flexDirection: 'row',
    alignItems: 'center',
  },
});
