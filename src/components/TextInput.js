import React, {forwardRef, useEffect, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  TextInput as Input,
  TouchableOpacity,
  View,
} from 'react-native';
import {IcCamera} from '../assets';
import {colors, scaleFont, scaleSize} from '../utils';
import Button from './Button';
import ErrorMessage from './ErrorMessage';
import Gap from './Gap';
import {TextRegular} from './Text';

const TextInput = forwardRef((props, ref) => {
  const [borderWidth, setBorderWidth] = useState(1);
  const [borderColor, setBorderColor] = useState(colors['Neutral-200']);

  const onFocus = () => {
    setBorderWidth(2);
    setBorderColor(colors['Primary-500']);
  };

  const onBlur = () => {
    setBorderWidth(1);
    setBorderColor(colors['Neutral-200']);
  };

  useEffect(() => {
    if (props.isError) {
      setBorderWidth(2);
      setBorderColor(colors['Error-500']);
    } else {
      setBorderWidth(1);
      setBorderColor(colors['Neutral-200']);
    }
  }, [props.isError]);

  switch (props.type) {
    case 'file':
      return (
        <>
          <View style={styles.row}>
            <TextRegular
              type="Text 14"
              text={props.label}
              color={colors['Neutral-800']}
            />
            {props.isRequired && (
              <TextRegular
                type="Text 14"
                text="*"
                color={colors['Error-500']}
              />
            )}
          </View>
          <Gap height={8} />
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.uploadWrapper(borderWidth, borderColor)}
            onPress={props.onPress}>
            <View style={styles.center}>
              <IcCamera />
              <Gap height={8} />
              <TextRegular
                type="Text 10"
                text="Upload maksimal 5mb"
                color={colors['Neutral-500']}
                style={styles.textCenter}
              />
            </View>
          </TouchableOpacity>
          <Gap height={4} />
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
          <Gap height={20} />
        </>
      );
    case 'location':
      return (
        <>
          <View style={styles.row}>
            <TextRegular
              type="Text 14"
              text={props.label}
              color={colors['Neutral-800']}
            />
            {props.isRequired && (
              <TextRegular
                type="Text 14"
                text="*"
                color={colors['Error-500']}
              />
            )}
          </View>
          <Gap height={8} />
          <View style={styles.row}>
            <View style={styles.containerLocation(borderWidth, borderColor)}>
              <Input
                {...props}
                ref={ref}
                style={styles.input}
                placeholderTextColor={colors['Neutral-500']}
                onFocus={onFocus}
                onBlur={onBlur}
                editable={false}
              />
            </View>
            <Gap width={12} />
            <Button text="Cek lokasi" onPress={props.onPress} />
          </View>
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
          <Gap height={20} />
        </>
      );
    default:
      return (
        <>
          <View style={styles.row}>
            <TextRegular
              type="Text 14"
              text={props.label}
              color={colors['Neutral-800']}
            />
            {props.isRequired && (
              <TextRegular
                type="Text 14"
                text="*"
                color={colors['Error-500']}
              />
            )}
          </View>
          <Gap height={8} />
          <View style={styles.container(borderWidth, borderColor)}>
            <Input
              {...props}
              ref={ref}
              style={styles.input}
              placeholderTextColor={colors['Neutral-500']}
              onFocus={onFocus}
              onBlur={onBlur}
            />
          </View>
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
          <Gap height={20} />
        </>
      );
  }
});

export default TextInput;

const styles = StyleSheet.create({
  container: (borderWidth, borderColor) => ({
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: scaleSize(42),
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(10),
    borderWidth,
    borderColor,
    borderRadius: scaleSize(8),
  }),
  containerLocation: (borderWidth, borderColor) => ({
    flexDirection: 'row',
    alignItems: 'center',
    width: '80%',
    height: scaleSize(42),
    paddingHorizontal: scaleSize(16),
    paddingVertical: scaleSize(10),
    borderWidth,
    borderColor,
    borderRadius: scaleSize(8),
    backgroundColor: colors['Neutral-200'],
  }),
  row: {
    flexDirection: 'row',
  },
  input: {
    flex: 1,
    height: scaleSize(42),
    fontFamily: 'OpenSans-Regular',
    fontSize: scaleFont(14),
    color: colors['Neutral-800'],
    lineHeight: 22.4,
  },
  uploadWrapper: (borderWidth, borderColor) => ({
    height: (Dimensions.get('window').height * 35) / 100,
    borderRadius: scaleSize(8),
    borderWidth,
    borderStyle: borderWidth === 2 ? 'solid' : 'dashed',
    borderColor,
    justifyContent: 'center',
    alignItems: 'center',
  }),
  center: {
    width: '25%',
    alignItems: 'center',
  },
  textCenter: {textAlign: 'center'},
});
