import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {scaleFont} from '../utils';

const getSize = type => {
  switch (type) {
    case 'Text 96':
      return 96;
    case 'Text 60':
      return 60;
    case 'Text 48':
      return 48;
    case 'Text 40':
      return 40;
    case 'Text 32':
      return 32;
    case 'Text 24':
      return 24;
    case 'Text 20':
      return 20;
    case 'Text 16':
      return 16;
    case 'Text 14':
      return 14;
    case 'Text 12':
      return 12;
    case 'Text 10':
      return 10;
  }
};

const getLineHeight = size => {
  switch (size) {
    case 96:
      return size * 1.2;
    case 60:
      return size * 1.2;
    case 48:
      return size * 1.2;
    case 40:
      return size * 1.4;
    case 32:
      return size * 1.4;
    case 24:
      return size * 1.4;
    case 20:
      return size * 1.6;
    case 16:
      return size * 1.6;
    case 14:
      return size * 1.6;
    case 12:
      return 16.34;
    case 10:
      return 13.62;
  }
};

export const TextBold = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text style={[styles.bold(size, color, lineHeight), style]}>{text}</Text>
  );
};

export const TextRegular = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text style={[styles.regular(size, color, lineHeight), style]}>{text}</Text>
  );
};

export const TextLight = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text style={[styles.light(size, color, lineHeight), style]}>{text}</Text>
  );
};

const styles = StyleSheet.create({
  bold: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Bold',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
  regular: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Regular',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
  light: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Light',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
});
