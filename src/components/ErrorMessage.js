import React from 'react';
import Animated, {BounceInLeft, Transition} from 'react-native-reanimated';
import {colors} from '../utils';
import {TextLight} from './Text';

const ErrorMessage = ({text, isError}) =>
  isError ? (
    <Animated.View entering={BounceInLeft.duration(1000)} layout={Transition}>
      <TextLight type="Text 12" text={text} color={colors['Error-500']} />
    </Animated.View>
  ) : null;

export default ErrorMessage;
