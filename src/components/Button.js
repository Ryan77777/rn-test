import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {TextBold} from './Text';
import {colors, scaleSize} from '../utils';

const Button = ({text, onPress, isPrimary, disable}) => {
  return disable ? (
    <View style={styles.buttonDisable(isPrimary)}>
      <TextBold
        type={isPrimary ? 'Text 16' : 'Text 10'}
        text={text}
        color={colors['White-0']}
      />
    </View>
  ) : (
    <TouchableOpacity
      style={styles.button(isPrimary)}
      activeOpacity={0.8}
      onPress={onPress}>
      <TextBold
        type={isPrimary ? 'Text 16' : 'Text 10'}
        text={text}
        color={colors['White-0']}
      />
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  button: isPrimary => ({
    backgroundColor: colors['Primary-500'],
    paddingVertical: isPrimary ? scaleSize(12) : scaleSize(4),
    paddingHorizontal: scaleSize(8),
    borderRadius: scaleSize(8),
    justifyContent: 'center',
    alignItems: 'center',
  }),
  buttonDisable: isPrimary => ({
    backgroundColor: colors['Neutral-300'],
    paddingVertical: isPrimary ? scaleSize(12) : scaleSize(4),
    paddingHorizontal: scaleSize(8),
    borderRadius: scaleSize(8),
    justifyContent: 'center',
    alignItems: 'center',
  }),
});
