import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splashscreen from '../pages/Splashscreen';
import List from '../pages/List';
import Create from '../pages/Create';

const Router = () => {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator
      screenOptions={{
        gestureEnabled: true,
        gestureDirection: 'horizontal',
        headerShown: false,
      }}>
      <Stack.Screen name="Splashscreen" component={Splashscreen} />
      <Stack.Screen name="List" component={List} />
      <Stack.Screen name="Create" component={Create} />
    </Stack.Navigator>
  );
};

export default Router;
